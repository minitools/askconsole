"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Promise = require("bluebird");
/**
 * @class AskConsole
 */
class AskConsole {
    /**
     * @constructor
     */
    constructor() {
        this.ANSWARE_NOT_ALLOWED_MESSAGE = 'Your answare was not an allowed asware, please enter an allowed answare \n';
        this.DEPRECATED_DEFAULT_RESPONSE_IN_QUESTION = 'Set default response between () in question is deprecated, please use askConsole.setDefaultResponse(\'key1\', \'answer\') instead';
        this.NO_DATA_RETURNED_BY_CONSOLE_MESSAGE = 'Please enter some data, otherweise we can\'t continue... \n';
        /**
         * @function startLot - start a lot of questions
         * @returns {Promise<Map<string,string>>}
         */
        this.startLot = () => {
            const responses = new Map();
            //@ts-ignore
            return Promise.resolve(this.questions)
                .each((question) => {
                return this.ask(question[1], question[0])
                    .then((response) => {
                    responses.set(question[0], response);
                });
            })
                .then(() => {
                return responses;
            });
        };
        /**
         * @function setQuestions
         * @returns {void}
         */
        this.setQuestion = (key, question) => {
            this.questions.set(key, question);
        };
        /**
         * @function getQuestions
         * @returns {map} this.questions
         */
        this.getQuestions = () => {
            return this.questions;
        };
        /**
         * @function setDefaultResponse
         * @returns {void}
         */
        this.setDefaultResponse = (key, defaultResponse) => {
            this.defaultResponses.set(key, defaultResponse);
        };
        /**
         * @function getDefaultResponse
         * @returns {string}
         */
        this.getDefaultResponse = (key) => {
            return this.defaultResponses.get(key);
        };
        /**
         * @function setDefaultResponse
         * @returns {void}
         */
        this.setAllowedResponses = (key, allowedResponses) => {
            this.allowedResponses.set(key, allowedResponses);
        };
        /**
         * @function setDefaultResponse
         * @returns {void}
         */
        this.getAllowedResponses = (key) => {
            return this.allowedResponses.get(key) || [];
        };
        /**
         * @function setDefaultResponse
         * @returns {boolean}
         */
        this.hasAllowedResponses = (key) => {
            return this.allowedResponses.has(key);
        };
        /**
         * @function jsUcfirst - capitalize the first chard of a string
         * @returns {string}
         */
        this.jsUcfirst = (stringForCapitalize) => {
            // tslint:disable-next-line
            return stringForCapitalize.charAt(0).toUpperCase() + stringForCapitalize.slice(1);
        };
        this.buildFormattedQuestion = (question, key) => {
            let formattedQuestion = question;
            // the question has a default response and accept only the defined allowed responses
            if (key && this.defaultResponses.has(key) && this.allowedResponses.has(key)) {
                const defaultResponse = this.defaultResponses.get(key) || '';
                const allowedResponse = this.allowedResponses.get(key) || [];
                const formatedAllowedResponses = allowedResponse.filter(response => response.toLowerCase() !== defaultResponse.toLowerCase())
                    //@ts-ignore  
                    //tslint:disable-next-line
                    .unshift(this.jsUcfirst(defaultResponse)).join('/');
                formattedQuestion = question + '(' + formatedAllowedResponses + ') :';
            }
            // the question has a default response and there are not allowed responses defined
            else if (key && this.defaultResponses.has(key)) {
                const defaultResponse = this.defaultResponses.get(key) || '';
                formattedQuestion = question + '(' + this.jsUcfirst(defaultResponse) + ') :';
            }
            // the question has not a default response but has allowed responses
            else if (key && this.allowedResponses.has(key)) {
                const allowedResponses = this.allowedResponses.get(key) || [];
                formattedQuestion = question + '(' + allowedResponses.join('/') + ') :';
            }
            return formattedQuestion;
        };
        /**
         * @function ask
         * @returns {Promise<string>}
         */
        this.ask = (question, key) => {
            return new Promise((resolve) => {
                const stdin = process.stdin;
                const stdout = process.stdout;
                const formattedQuestion = this.buildFormattedQuestion(question, key);
                stdin.resume();
                stdout.write(`${formattedQuestion}: `);
                stdin.once('data', (data) => {
                    // tslint:disable-next-line
                    data = data.toString().trim();
                    if (!data || data === '') {
                        const defaultResponse = key ? this.getDefaultResponse(key) :
                            question.match(/(?<=\()(.*?)(?=\))/);
                        if (typeof defaultResponse === 'string') {
                            data = defaultResponse;
                        }
                        else {
                            data = defaultResponse ? defaultResponse[0] : '';
                            if (key
                                && data !== ''
                                // tslint:disable-next-line
                                && data.toLowerCase() !== (this.allowedResponses.get(key) || []).join('/').toLowerCase()) {
                                console.warn(this.DEPRECATED_DEFAULT_RESPONSE_IN_QUESTION);
                            }
                        }
                        if (!data || data === '') {
                            stdout.write(this.NO_DATA_RETURNED_BY_CONSOLE_MESSAGE);
                            resolve(this.ask(question));
                        }
                        else {
                            resolve(data);
                        }
                    }
                    else {
                        if (key && this.hasAllowedResponses(key)) {
                            const allowedResponses = this.getAllowedResponses(key);
                            const responseMatchAllowedResponses = allowedResponses
                                .filter((allowedResponse) => allowedResponse === data)
                                .length > 0;
                            if (responseMatchAllowedResponses) {
                                resolve(data);
                            }
                            else {
                                stdout.write(this.ANSWARE_NOT_ALLOWED_MESSAGE);
                                resolve(this.ask(question));
                            }
                        }
                        else {
                            resolve(data);
                        }
                    }
                });
            });
        };
        this.allowedResponses = new Map();
        this.defaultResponses = new Map();
        this.questions = new Map();
    }
}
exports.AskConsole = AskConsole;
//# sourceMappingURL=askConsole.js.map