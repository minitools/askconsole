"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const askConsole_1 = require("../askConsole");
test('test for askConsole', () => {
    const askConsole = new askConsole_1.AskConsole();
    askConsole.setQuestion('keyQuestion', 'test question');
    // tslint:disable-next-line
    expect(askConsole.getQuestions().size).toBe(1);
});
//# sourceMappingURL=askConsole.test.js.map