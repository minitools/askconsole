import * as Promise from 'bluebird';
/**
 * @class AskConsole
 */
export declare class AskConsole {
    private readonly ANSWARE_NOT_ALLOWED_MESSAGE;
    private readonly DEPRECATED_DEFAULT_RESPONSE_IN_QUESTION;
    private readonly NO_DATA_RETURNED_BY_CONSOLE_MESSAGE;
    private allowedResponses;
    private defaultResponses;
    private questions;
    /**
     * @constructor
     */
    constructor();
    /**
     * @function startLot - start a lot of questions
     * @returns {Promise<Map<string,string>>}
     */
    startLot: () => Promise<Map<string, string>>;
    /**
     * @function setQuestions
     * @returns {void}
     */
    setQuestion: (key: string, question: string) => void;
    /**
     * @function getQuestions
     * @returns {map} this.questions
     */
    getQuestions: () => Map<string, string>;
    /**
     * @function setDefaultResponse
     * @returns {void}
     */
    setDefaultResponse: (key: string, defaultResponse: string) => void;
    /**
     * @function getDefaultResponse
     * @returns {string}
     */
    getDefaultResponse: (key: string) => string | undefined;
    /**
     * @function setDefaultResponse
     * @returns {void}
     */
    setAllowedResponses: (key: string, allowedResponses: string[]) => void;
    /**
     * @function setDefaultResponse
     * @returns {void}
     */
    getAllowedResponses: (key: string) => string[];
    /**
     * @function setDefaultResponse
     * @returns {boolean}
     */
    hasAllowedResponses: (key: string) => boolean;
    /**
     * @function jsUcfirst - capitalize the first chard of a string
     * @returns {string}
     */
    jsUcfirst: (stringForCapitalize: string) => string;
    buildFormattedQuestion: (question: string, key?: string | undefined) => string;
    /**
     * @function ask
     * @returns {Promise<string>}
     */
    ask: (question: string, key?: string | undefined) => Promise<string>;
}
