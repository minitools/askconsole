##### Install

    npm i askconsole

#### Usage

```js    
   import {AskConsole} from 'askconsole';
   const askConsole = new AskConsole();

   askConsole.setQuestion('key1','Please my software need an input:');
   askConsole.setDefaultResponse('key1','yes'); //optional
   askConsole.setAllowedResponses('key1',['yes','No','I don\'t know']);// optional
   // that would generate a question like:
   // Please my software need an input (Yes/no/i don't know):
   // note that the default response is capitalized
   
   askConsole.setQuestion('key2','o sorry my software need still some info:');
   ...more
	
   askConsole.startLot()
	.then((responses) => {
		console.log(responses.get('key1');
		console.log(responses.get('key2'))
		...more
		
		// do somthing with the responses

	});
```
