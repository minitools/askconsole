import * as Promise from 'bluebird';

/**
 * @class AskConsole
 */
export class AskConsole {

  private readonly ANSWARE_NOT_ALLOWED_MESSAGE = 'Your answare was not an allowed asware, please enter an allowed answare \n';
  private readonly DEPRECATED_DEFAULT_RESPONSE_IN_QUESTION = 'Set default response between () in question is deprecated, please use askConsole.setDefaultResponse(\'key1\', \'answer\') instead';
  private readonly NO_DATA_RETURNED_BY_CONSOLE_MESSAGE = 'Please enter some data, otherweise we can\'t continue... \n';
  
  private allowedResponses: Map<string, string[]>;
  private defaultResponses: Map<string, string>;
  private questions: Map<string, string>;
  
  /**
   * @constructor
   */
  constructor() {
    this.allowedResponses = new Map();
    this.defaultResponses = new Map();
    this.questions = new Map();
  }

  /**
   * @function startLot - start a lot of questions
   * @returns {Promise<Map<string,string>>}
   */
  startLot = (): Promise<Map<string, string>> => {
        const responses: Map<string, string> = new Map();
        //@ts-ignore
        return Promise.resolve(this.questions)
            .each((question: string) => {
              return this.ask(question[1], question[0])
                  .then((response: string) => {
                    responses.set(question[0], response);
                  });
            })
            .then(() => {
              return responses;
            });
  }

  /**
   * @function setQuestions
   * @returns {void}
   */
  setQuestion = (key: string, question: string) => {
        this.questions.set(key, question);
  }

  /**
   * @function getQuestions
   * @returns {map} this.questions
   */
  getQuestions = (): Map<string,string> => {
    return this.questions;
}

  /**
   * @function setDefaultResponse
   * @returns {void}
   */
  setDefaultResponse = (key: string, defaultResponse: string) => {
        this.defaultResponses.set(key, defaultResponse);
  }

  /**
   * @function getDefaultResponse
   * @returns {string}
   */
  getDefaultResponse = (key: string) => {
        return this.defaultResponses.get(key);
  }

  /**
   * @function setDefaultResponse
   * @returns {void}
   */
  setAllowedResponses = (key: string, allowedResponses: string[]) => {
        this.allowedResponses.set(key, allowedResponses);
  }

  /**
   * @function setDefaultResponse
   * @returns {void}
   */
  getAllowedResponses = (key: string): string[] => {
        return this.allowedResponses.get(key) || [];
  }

  /**
   * @function setDefaultResponse
   * @returns {boolean}
   */
  hasAllowedResponses = (key: string): boolean => {
        return this.allowedResponses.has(key);
  }
  
  /**
   * @function jsUcfirst - capitalize the first chard of a string
   * @returns {string}
   */
  jsUcfirst = (stringForCapitalize: string): string => {
    // tslint:disable-next-line
    return stringForCapitalize.charAt(0).toUpperCase() + stringForCapitalize.slice(1);
  }

  /**
   * @function buildFormattedQuestion
   * @returns {string}
   */
  buildFormattedQuestion = (question: string, key?: string): string => {
    
    let formattedQuestion = question;
    // the question has a default response and accept only the defined allowed responses
    if (key && this.defaultResponses.has(key) && this.allowedResponses.has(key)) {
      const defaultResponse = this.defaultResponses.get(key) || '';
      const allowedResponse = this.allowedResponses.get(key) || [];
      const formatedAllowedResponses = 
        allowedResponse.filter(response => response.toLowerCase() !== defaultResponse.toLowerCase())
          //@ts-ignore  
          //tslint:disable-next-line
          .unshift(this.jsUcfirst(defaultResponse)).join('/');
      formattedQuestion = question + ' (' + formatedAllowedResponses + ')';
    }
    // the question has a default response and there are not allowed responses defined
    else if (key && this.defaultResponses.has(key)) {
      const defaultResponse = this.defaultResponses.get(key) || '';
      formattedQuestion = question + ' (' + this.jsUcfirst(defaultResponse) + ')';
    }
    // the question has not a default response but has allowed responses
    else if (key && this.allowedResponses.has(key)) {
      const allowedResponses = this.allowedResponses.get(key) || [];
      formattedQuestion = question + ' (' + allowedResponses.join('/') + ')';
    }
    
    return formattedQuestion;
  };

  /**
   * @function ask
   * @returns {Promise<string>}
   */
  ask = (question: string, key?: string): Promise<string> => {
    return new Promise((resolve) => {
      const stdin = process.stdin;
      const stdout = process.stdout;
      const formattedQuestion = this.buildFormattedQuestion(question, key);
      
      stdin.resume();
      stdout.write(`${formattedQuestion}:`);
      stdin.once('data', (data) => {
        // tslint:disable-next-line
        data = data.toString().trim();
        if (!data || data === '') {
          const defaultResponse = key ? this.getDefaultResponse(key) :
                                        question.match(/(?<=\()(.*?)(?=\))/);
          if (typeof defaultResponse === 'string') {
            data = defaultResponse;
          } 
          else {
            data = defaultResponse ? defaultResponse[0] : '';
            if (key 
              && data !== '' 
              // tslint:disable-next-line
              && data.toLowerCase() !== (this.allowedResponses.get(key) || []).join('/').toLowerCase()) {
              console.warn(this.DEPRECATED_DEFAULT_RESPONSE_IN_QUESTION);
            }
          }

          if (!data || data === '') {
            stdout.write(this.NO_DATA_RETURNED_BY_CONSOLE_MESSAGE);
            resolve(this.ask(question));
          } 
          else {
            resolve(data);
          }
        } 
        else {
          if (key && this.hasAllowedResponses(key)) {
            const allowedResponses = this.getAllowedResponses(key);
            const responseMatchAllowedResponses =
                allowedResponses
                    .filter((allowedResponse) => allowedResponse === data)
                    .length > 0;

            if (responseMatchAllowedResponses) {
              resolve(data);
            } 
            else {
              stdout.write(this.ANSWARE_NOT_ALLOWED_MESSAGE);
              resolve(this.ask(question));
            }
          } 
          else {
            resolve(data);
          }
        }
      });
    });
  }
}
