
import {AskConsole} from '../askConsole';

test('test for askConsole', () => {
  const askConsole = new AskConsole();

  askConsole.setQuestion('keyQuestion', 'test question');
  // tslint:disable-next-line
  expect(askConsole.getQuestions().size).toBe(1);
});
